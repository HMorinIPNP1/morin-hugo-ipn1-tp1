import matplotlib
import matplotlib.pyplot as plt
import numpy as np

t = np.arange(-5.0, 5.0, 0.01)
s = np.exp(-1*abs(t)) * np.exp(1j*2*np.pi*5*t)
m = abs(s)

fourier = np.fft.fft(s)
module = abs(fourier)
n = s.size
freq = np.fft.fftfreq(n, 0.01)

fig, ax = plt.subplots(4)
ax[0].plot(t,s, label="signal")
ax[1].plot(freq, module, label="module")
ax[2].plot(freq, fourier.real, label="real")
ax[3].plot(freq, fourier.imag, label="imag")
ax[0].legend()
ax[1].legend()
ax[2].legend()
ax[3].legend()
plt.show()
fig.savefig("Ex12.png")
