import matplotlib
import matplotlib.pyplot as plt
import numpy as np

t = np.arange(-5.0, 5.0, 0.01)
s = np.exp(-1*abs(t))

fig, ax = plt.subplots()
ax.plot(t, s)

ax.set(xlabel='time (s)', ylabel='voltage (mV)', title='About as simple as it gets, folks')
ax.grid()

fig.savefig("Ex7.png")
plt.show()
