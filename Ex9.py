import matplotlib
import matplotlib.pyplot as plt
import numpy as np

t = np.arange(-5.0, 5.0, 0.01)
s = np.exp(-1*abs(t))
m = abs(s)

fourier = np.fft.fft(s)
a = np.angle(fourier)
module = abs(fourier)
n = s.size
freq = np.fft.fftfreq(n, 0.01)

fig, ax = plt.subplots(3)
ax[0].plot(t,s, label="signal")
ax[1].plot(freq, module, label="module")
ax[2].plot(freq, a, label="angle")
ax[0].legend()
ax[1].legend()
ax[2].legend()
plt.show()
fig.savefig("Ex9.png")
