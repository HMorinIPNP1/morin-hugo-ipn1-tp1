import matplotlib
import matplotlib.pyplot as plt
import numpy as np

t = np.arange(-5.0, 5.0, 0.01)
s = np.exp(-1*abs(t))

fourier = np.fft.fft(s)
n = s.size
freq = np.fft.fftfreq(n, 0.01)

fig, ax = plt.subplots(2)
ax[0].plot(t,s)
ax[1].plot(freq, abs(fourier.real), label="real")
ax[1].plot(freq, fourier.imag, label="imag")
ax[0].legend()
ax[1].legend()
plt.show()
fig.savefig("Ex8.png")
