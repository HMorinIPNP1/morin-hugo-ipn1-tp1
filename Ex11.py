import matplotlib
import matplotlib.pyplot as plt
import numpy as np

t = np.arange(-5.0, 5.0, 0.01)
s = np.exp(-1*abs(t))

ifourier = np.fft.ifft(s)
n = s.size
freq = np.fft.fftfreq(n, 0.01)

fig, ax = plt.subplots(2)
ax[0].plot(t,s, label="signal")
ax[1].plot(freq, ifourier.real, label="real")
ax[0].legend()
ax[1].legend()
plt.show()
fig.savefig("Ex11.png")
